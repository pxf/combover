package.path = package.path .. ";?/init.lua;"

local combover = require("engine")
local gamefont = require("game/gamefont")

combover:init({
	title 	   = "Cool window",
    win_w      = 512,
    win_h      = 512,
    fullscreen = false,
    boot 	   = nil,
    assets     = {
    	window_shader = combover.new_asset( "shader", { "examples/example_assets/forward.vs", "examples/example_assets/forward.fs", true } )
	}
})

local screen = {}

screen.init = function(self)
	self.dim = { game.settings.win_w / 2, game.settings.win_h / 2 }
	self.time = 0
end

screen.update = function( self, game, dt, vis )
	self.time = self.time + dt

	self.dim[1] = math.sin(self.time) + self.dim[1]
	self.dim[2] = math.cos(self.time) + self.dim[2]
end

screen.draw = function( self, game, dt, vis )
	gamefont.draw( self.dim[1], self.dim[2], 16, "Hello!", nil, vis)
end

combover:push_screen( screen )
combover:run()