uniform float download;
uniform float pos_x;
uniform vec3 color;

uniform sampler2D tex0;

varying vec2 v_uv0;

void main()
{
	gl_FragColor = vec4(color, 1.0);
}