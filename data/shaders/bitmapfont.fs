uniform sampler2D tex0;
uniform float vis;

varying vec2 v_uv0;
varying vec3 v_color;

void main()
{
	vec4 texcol = texture2D( tex0, v_uv0 );
	//gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
	gl_FragColor.rgb = texcol.rgb * v_color;// + vec4(0.4, 0.0, 0.0, 0.0);
	//gl_FragColor.rgb = v_color;// + vec4(0.4, 0.0, 0.0, 0.0);
	gl_FragColor.a = texcol.a * vis;
	//gl_FragColor.a = 1.0;
	//gl_FragColor = vec4(color, 1.0);
}