attribute vec4 position;

uniform float download;
uniform float pos_x;
uniform vec3 color;
uniform mat4 pmtx;

varying vec2 v_uv0;

void main()
{
	v_uv0 = (position.st + 1.0) * 0.5;

	vec3 pos = position.xyz;
	pos.x *= download;
	pos.x += pos_x;

	gl_Position = pmtx * vec4(pos, 1.0);
	
}
