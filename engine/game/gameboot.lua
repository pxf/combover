local gfx = require("gfx/gfx")
local gl  = require( "ffi/OpenGL" ) 
local gamefont = require("game/gamefont")
local boot = {}

boot.init = function(self)
	print("Booting up engine..")

	self.preload_string_history = {}

	table.insert(self.preload_string_history, 1, "Preloading assets..")

	local q_w  = 400
	local q_h  = 12
	local q_x0 = game.settings.win_w / 2 - q_w / 2
	local q_y0 = game.settings.win_h / 2 + q_h / 2
	local q_x1 = game.settings.win_w / 2 + q_w / 2
	local q_y1 = game.settings.win_h / 2 - q_h / 2

	progress_shader = gfx.shader("data/shaders/preload.vs","data/shaders/preload.fs",true)
	progress_quad   = gfx.geometry( {
		buffers = {
			position = {
				size = 4,
				data = { 0, q_y0, 0, 0,
						 q_w, q_y1, 0, 0,
						 0, q_y1, 0, 0,

						 0, q_y0, 0, 0,
						 q_w, q_y0, 0, 0,
						 q_w, q_y1, 0, 0
				}
			}
		}
	})
end

boot.update = function(self,game,dt,vis)
	-- if self.progress >= 1 then return end

	local asset_count  = 0
	local asset_loaded = 0

	for k,v in pairs( assets.preload ) do 
		asset_count = asset_count + 1	
	end

	for k,v in pairs( assets.preload ) do 
		if not v.loaded then
			table.insert(self.preload_string_history, 1, "Loading game asset: " .. tostring(k))

			assets.preload[k] = v.load( unpack(v.args) )
			assets.preload[k].loaded = true
			assets[k] = assets.preload[k]
			break
		else
			asset_loaded = asset_loaded + 1
		end
	end

	self.progress = asset_count / asset_loaded
end

boot.draw = function(self,game,dt,vis)
	gl.glDisable( gl.GL_DEPTH_TEST )
	gl.glViewport(0, 0, game.settings.win_w, game.settings.win_h)
	gl.glClear( gl.GL_COLOR_BUFFER_BIT )
	gl.glEnable( gl.GL_BLEND )
	gl.glBlendFunc( gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA )
	gl.glBlendEquation( gl.GL_FUNC_ADD )

    local start_y = game.settings.win_h / 2 - 24 - 12
    for k,v in pairs(self.preload_string_history) do
    	gamefont.draw( game.settings.win_w / 2 - 200, start_y, 16, v, nil, vis)
    	start_y = start_y - 12
    end

    local progress_string = ""
    local preload_string  = ""

	progress_shader:bind()
	progress_shader:setuniform("mat4", "pmtx", mat4.ortho(0, game.settings.win_w, game.settings.win_h, 0, 0, 1))
	progress_shader:setuniform("f", "pos_x", game.settings.win_w / 2 - 400 / 2)

	-- draw progress bg
	progress_shader:setuniform("f", "download", 1.0)
	progress_shader:setuniform("fv", "color", {0.12, 0.12, 0.12})
	progress_quad:bind(progress_shader)
	progress_quad:draw()
	progress_quad:unbind()

	-- draw progress bar
	progress_shader:setuniform("fv", "color", {1.0, 1.0, 1.0})
	progress_quad:bind(progress_shader)
	progress_quad:draw()
	progress_quad:unbind()

	progress_shader:unbind()
end

return boot