local gfx = require("gfx/gfx")

gamefont = {}
gamefont.font_width = 8
gamefont.font_size  = 16
gamefont.font_dim   = 256

function gamefont.draw( x, y, size, txt, align, vis )

	if not assets.font_texture then assets.font_texture = gfx.texture("data/textures/bitmapfont.png") end
	if not assets.font_batch  then assets.font_batch = gfx.quadbatch() end
	if not	assets.font_shader then assets.font_shader = gfx.shader("data/shaders/bitmapfont.vs", "data/shaders/bitmapfont.fs", true) end
		
		local qb = assets["font_batch"]
		local x_r = 0
		qb:begin()

		if (vis == nil) then
			vis = 1.0
		end


		local color = {1,1,1}
		local waiting_color = false
		for i=1,#txt do



			-- calc uv coords
			local s = 0
			local t = 0

			local tex_delta = (1.0 / gamefont.font_dim) * gamefont.font_size
			local b = string.byte(txt, i)
			s = (b % gamefont.font_size) * tex_delta
			t = math.floor(b / gamefont.font_size) * tex_delta

			if (waiting_color) then
				if (b == string.byte("0", 1)) then
					color = {1,0,0}
				elseif b == string.byte("1", 1) then
					color = {0,1,0}
				elseif b == string.byte("2", 1) then
					color = {0,0,1}
				else
					color = {1,1,1}
				end

				waiting_color = false
			else

				if b == string.byte("^", 1) then
					waiting_color = true
					--print("chaning color")
				else

					qb:setcoords(s, t, s + tex_delta, t, s + tex_delta, t + tex_delta, s, t + tex_delta)
					qb:setnormals(color)
					qb:addcentered(x_r, 0, size / gamefont.font_size * gamefont.font_size, size / gamefont.font_size * gamefont.font_size)
					
					x_r = x_r + size / gamefont.font_size * gamefont.font_width

				end
			end

		end

		qb:finish()

		local mtx = mat4.identity()
		local str_size = #txt * size / gamefont.font_size * gamefont.font_width

		if (align == 1) then
			mat4.translate( mtx, { - str_size, 0, 0}, mtx )
		elseif (align == 0) then
			mat4.translate( mtx, { - str_size / 2.0, 0, 0}, mtx )
		end
		mat4.translate( mtx, {x, y, 0}, mtx )


		assets["font_shader"]:bind()
		assets["font_shader"]:setuniform("mat4", "pmtx", mat4.ortho(0, game.settings.win_w, game.settings.win_h, 0, 0, 1))
		assets["font_shader"]:setuniform("mat4", "mmtx", mtx)
		assets["font_shader"]:setuniform("i", "tex0", 0)
		assets["font_shader"]:setuniform("f", "vis", vis)

		assets["font_texture"]:bind(0)

		qb:draw(assets["font_shader"])
		
		assets["font_texture"]:unbind()

		assets["font_shader"]:unbind()

end


return gamefont