local gfx = require("gfx/gfx")

return function(asset_type,extra_args)
	local asset = { loaded = false }

	if asset_type == "shader" then 
		print("new shader asset..")
		asset.load = gfx.shader
		asset.args = extra_args

		if asset.args[3] == nil then
			asset.args[3] = true
		end
	elseif asset_type == "texture" then 
		asset.load = gfx.texture
		asset.args = extra_args
	end

	-- todo: add more if needed

	return asset
end