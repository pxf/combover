local ffi 	= require( "ffi" )
local gl 	= require( "ffi/OpenGL" )
local utils = require( "utils" )

__geometry = function( params, workdir )
	local g = {}

	g.buffers 		= {}
	g.primitivetype = gl.GL_TRIANGLES
	g.facebuffer 	= nil
	g.drawlength 	= 0
	g.buffersbound	= {}

	extend( g, params )

	if (g.buffers.url) then
		local buffers 	= nil
		local geo_data 	= load_external_geometry( workdir .. g.buffers.url )

		g.buffers 		= geo_data.buffers
		g.primitiveType = gl.GL_TRIANGLES
	end

	g.destroy = function(self)
		for k,v in pairs(self.buffers) do
			local bufptr = ffi.new( "const GLuint[1]", v.glbuffer )

			gl.glDeleteBuffers( 1, bufptr )
			self.buffers[k].data = nil
		end

		return self
	end

	g.build = function(self)
		local drawLength = math.huge

		for k,v in pairs(self.buffers) do

			if not v.glbuffer then 
				local i_ptr = ffi.new( "int[1]", 0 )
				gl.glGenBuffers(1,i_ptr)

				v.glbuffer 	= i_ptr[0]
				v.usage 	= v.usage and v.usage or gl.GL_STATIC_DRAW
			end

			if v.elements == 1 then
				error("geometry.elements Not implemented yet!!")
			else
				gl.glBindBuffer(gl.GL_ARRAY_BUFFER, v.glbuffer)

				if drawLength > #v.data / v.size then
					drawLength = #v.data / v.size
				end

				local ptr_decl = "float[" .. #v.data .. "]"
				local data_ptr = ffi.new( ptr_decl, {} )

				data_ptr = fillptrfromtable( data_ptr, v.data, #v.data )

				gl.glBufferData( gl.GL_ARRAY_BUFFER, #v.data * 4, ffi.cast("void*",data_ptr), v.usage )
				gl.glBindBuffer( gl.GL_ARRAY_BUFFER, 0 )
			end
		end

		if self.facebuffer then 
			drawLength = #self.facebuffer.data
		end

		self.drawlength = drawLength

		return self
	end

	g.bind = function(self, shader, buffers )
		gl.glEnableClientState(gl.GL_VERTEX_ARRAY)

		if buffers == nil then buffers = self.buffers end
		for buf,bufv in pairs(buffers) do 
			for k,v in pairs( self.buffers ) do 
				if ( k == buf or k == bufv) then

					local b = v
					if not b.elements then
						local buffername 	= b.buffername and b.buffername or k
						local loc 			= shader:getattriblocation( buffername )

						if loc >= 0 then
							gl.glEnableVertexAttribArray(loc)
							gl.glBindBuffer( gl.GL_ARRAY_BUFFER, b.glbuffer )

							shader:vertexattrib( "p", buffername, b.size, gl.GL_FLOAT, false, 0 )

							self.buffersbound[buffername] = loc
						end
					end
				end
			end
		end

		if self.facebuffer then
			gl.glBindBuffer( gl.ELEMENT_ARRAY_BUFFER, self.facebuffer.glbuffer )
		end

		return self
	end

	g.unbind = function(self)

		for buf,loc in pairs( self.buffersbound ) do
			if ( loc >= 0 ) then 
				gl.glDisableVertexAttribArray( loc )
			end
		end

		self.buffersbound = {}

		if self.facebuffer then 
			gl.glBindBuffer( gl.GL_ELEMENT_ARRAY_BUFFER, 0)
		end

		gl.glBindBuffer( gl.GL_ARRAY_BUFFER, 0 )
		gl.glDisableClientState(gl.GL_VERTEX_ARRAY)

		return self
	end

	g.draw = function(self)

		if self.faceBuffer then
			error("geometry.facebuffer.draw not implemented yet")
		else
			gl.glDrawArrays( self.primitivetype, 0, self.drawlength)
		end

		return self
	end

	return g:build()
end