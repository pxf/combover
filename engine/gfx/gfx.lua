package.path = package.path .. ";./engine/gfx/?.lua;"

require("shader")
require("geometry")
require("texture")
require("framebuffer")
require("quadbatch")

gfx 			= {}
gfx.shader 		= __shader
gfx.geometry 	= __geometry
gfx.texture 	= __texture
gfx.framebuffer = __framebuffer
gfx.quadbatch 	= __quadbatch

return gfx