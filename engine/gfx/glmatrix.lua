-- adopted from 'glmatrix' @ https://github.com/toji/gl-matrix

vec2  = {}
vec3  = {}
vec4  = {}
mat3  = {}
mat4  = {}
quat4 = {}

-----------------
-- VEC3
----------------
function vec3.identity()
    return {1,1,1}
end

function vec3.create( v )
    if not v then return {0,0,0} end

    return { v[1], v[2], v[3] }
end

function vec3.identity(v)
    v = vec3.create(v)

    v[1] = 1
    v[2] = 1
    v[3] = 1

    return v
end

function vec3.length(vec) 

    local x = vec[1]
    local y = vec[2]
    local z = vec[3]

    return math.sqrt( x * x + y * y + z * z )
end

function vec3.dot( vec, vec2 ) 
    return vec[1] * vec2[1] + vec[2] * vec2[2] + vec[3] * vec2[3]
end


function vec3.scale( vec, val, dest ) 

    if not dest or vec == dest then
        vec[1] = vec[1] * val
        vec[2] = vec[2] * val
        vec[3] = vec[3] * val
        return vec
    end

    dest[1] = vec[1] * val
    dest[2] = vec[2] * val
    dest[3] = vec[3] * val
    return dest
end

function vec3.subtract(vec,vec2,dest) 

    if not dest or vec == dest then
        vec[1] = vec[1] - vec2[1];
        vec[2] = vec[2] - vec2[2];
        vec[3] = vec[3] - vec2[3];
        return vec
    end

    dest[1] = vec[1] - vec2[1];
    dest[2] = vec[2] - vec2[2];
    dest[3] = vec[3] - vec2[3];

    return dest
end

function vec3.add(vec,vec2,dest)

    if not dest or vec == dest then
        vec[1] = vec[1] + vec2[1];
        vec[2] = vec[2] + vec2[2];
        vec[3] = vec[3] + vec2[3];
        return vec
    end

    dest[1] = vec[1] + vec2[1]
    dest[2] = vec[2] + vec2[2]
    dest[3] = vec[3] + vec2[3]

    return dest
end

function vec3.cross(vec,vec2,dest) 

    if not dest then dest = vec end

    local x = vec[1]
    local y = vec[2]
    local z = vec[3]

    local x2 = vec2[1]
    local y2 = vec2[2]
    local z2 = vec2[3]

    dest[1] = y * z2 - z * y2
    dest[2] = z * x2 - x * z2
    dest[3] = x * y2 - y * x2
    return dest
end

function vec3.multiply(vec,vec2,dest)
    if not dest or vec == dest then
        vec[1] = vec[1] * vec2[1];
        vec[2] = vec[2] * vec2[2];
        vec[3] = vec[3] * vec2[3];
        return vec
    end

    dest[1] = vec[1] * vec2[1];
    dest[2] = vec[2] * vec2[2];
    dest[3] = vec[3] * vec2[3];

    return dest
end

function vec3.normalize(vec, dest)
    if not dest then dest = vec end

    local x     = vec[1]
    local y     = vec[2]
    local z     = vec[3]
    local len   = math.sqrt(x * x + y * y + z * z);

    if not len then
        dest[1] = 0;
        dest[2] = 0;
        dest[3] = 0;
        return dest
    elseif len == 1 then
        dest[1] = x;
        dest[2] = y;
        dest[3] = z;
        return dest;
    end

    len = 1 / len;

    dest[1] = x * len;
    dest[2] = y * len;
    dest[3] = z * len;

    return dest;
end

-----------------
-- MAT4
----------------

function mat4.print(m)
	if not m then return end

	local output = "{ "

	for k,v in pairs(m) do 
		output = output .. v 
		if k ~= #m then output = output .. ", " end
	end

	output = output .. " }"
	print(output)
end

function mat4.print2(m)
	if not m then return end

	local output = "{ "

	for k,v in pairs(m) do 
		output = output .. v 
		if k ~= #m then
			output = output .. ", "
			if k % 4 == 0 then output = output .. "\n" end
		end
		
	end

	output = output .. " }"
	print(output)
end

function mat4.create(m)
	m = m and m or {}
	local dest = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 }

	for k,v in pairs(m) do
		dest[k] = v
	end

	return dest
end

function mat4.identity(m)
	if not m then
		m = mat4.create()
	end

	m[1] = 1
	m[2] = 0
	m[3] = 0
	m[4] = 0
	m[5] = 0
	m[6] = 1
	m[7] = 0
	m[8] = 0
	m[9] = 0
	m[10] = 0
	m[11] = 1
	m[12] = 0
	m[13] = 0
	m[14] = 0
	m[15] = 0
	m[16] = 1

	return m
end

function mat4.inverse(m,dest)
	if not dest then dest = m end

	local a00 = m[1]
	local a01 = m[2]
	local a02 = m[3]
	local a03 = m[4]
	local a10 = m[5]
	local a11 = m[6]
	local a12 = m[7]
	local a13 = m[8]
	local a20 = m[9]
	local a21 = m[10]
	local a22 = m[11]
	local a23 = m[12]
	local a30 = m[13]
	local a31 = m[14]
	local a32 = m[15]
	local a33 = m[16]

    local 	b00 = a00 * a11 - a01 * a10
	local   b01 = a00 * a12 - a02 * a10
	local   b02 = a00 * a13 - a03 * a10
	local   b03 = a01 * a12 - a02 * a11
	local   b04 = a01 * a13 - a03 * a11
	local   b05 = a02 * a13 - a03 * a12
	local   b06 = a20 * a31 - a21 * a30
	local   b07 = a20 * a32 - a22 * a30
	local   b08 = a20 * a33 - a23 * a30
	local   b09 = a21 * a32 - a22 * a31
	local   b10 = a21 * a33 - a23 * a31
	local   b11 = a22 * a33 - a23 * a32

	local d = (b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06)
    local invDet

	if not d or d == 0 then return nil end

	invDet = 1 / d;

	dest[1] = (a11 * b11 - a12 * b10 + a13 * b09) * invDet;
	dest[2] = (-a01 * b11 + a02 * b10 - a03 * b09) * invDet;
	dest[3] = (a31 * b05 - a32 * b04 + a33 * b03) * invDet;
	dest[4] = (-a21 * b05 + a22 * b04 - a23 * b03) * invDet;
	dest[5] = (-a10 * b11 + a12 * b08 - a13 * b07) * invDet;
	dest[6] = (a00 * b11 - a02 * b08 + a03 * b07) * invDet;
	dest[7] = (-a30 * b05 + a32 * b02 - a33 * b01) * invDet;
	dest[8] = (a20 * b05 - a22 * b02 + a23 * b01) * invDet;
	dest[9] = (a10 * b10 - a11 * b08 + a13 * b06) * invDet;
	dest[10] = (-a00 * b10 + a01 * b08 - a03 * b06) * invDet;
	dest[11] = (a30 * b04 - a31 * b02 + a33 * b00) * invDet;
	dest[12] = (-a20 * b04 + a21 * b02 - a23 * b00) * invDet;
	dest[13] = (-a10 * b09 + a11 * b07 - a12 * b06) * invDet;
	dest[14] = (a00 * b09 - a01 * b07 + a02 * b06) * invDet;
	dest[15] = (-a30 * b03 + a31 * b01 - a32 * b00) * invDet;
	dest[16] = (a20 * b03 - a21 * b01 + a22 * b00) * invDet;

	return dest;
end

function mat4.translate(mat,vec,dest) 
    local x = vec[1]
    local y = vec[2]
    local z = vec[3]
    local a00, a01, a02, a03,
          a10, a11, a12, a13,
          a20, a21, a22, a23

    if not dest or mat == dest then
        mat[13] = mat[1] * x + mat[5] * y + mat[9] * z + mat[13];
        mat[14] = mat[2] * x + mat[6] * y + mat[10] * z + mat[14];
        mat[15] = mat[3] * x + mat[7] * y + mat[11] * z + mat[15];
        mat[16] = mat[4] * x + mat[8] * y + mat[12] * z + mat[16];

        return mat 
    end

    a00 = mat[1]; a01 = mat[2]; a02 = mat[3]; a03 = mat[4];
    a10 = mat[5]; a11 = mat[6]; a12 = mat[7]; a13 = mat[8];
    a20 = mat[9]; a21 = mat[10]; a22 = mat[11]; a23 = mat[12];

    dest[1] = a00; dest[2] = a01; dest[3] = a02; dest[4] = a03;
    dest[5] = a10; dest[6] = a11; dest[7] = a12; dest[8] = a13;
    dest[9] = a20; dest[10] = a21; dest[11] = a22; dest[12] = a23;

    dest[13] = a00 * x + a10 * y + a20 * z + mat[13];
    dest[14] = a01 * x + a11 * y + a21 * z + mat[14];
    dest[15] = a02 * x + a12 * y + a22 * z + mat[15];
    dest[16] = a03 * x + a13 * y + a23 * z + mat[16];
end


function mat4.multiplyVec3(mat,vec,dest)
    if not dest then dest = vec end

    local x = vec[1]
    local y = vec[2]
    local z = vec[3]

    dest[1] = mat[1] * x + mat[5] * y + mat[9] * z + mat[13];
    dest[2] = mat[2] * x + mat[6] * y + mat[10] * z + mat[14];
    dest[3] = mat[3] * x + mat[7] * y + mat[11] * z + mat[15];

    return dest;
end

function mat4.multiply(mat,mat2,dest)
	if not dest then dest = mat end

    local a00 = mat[ 1]
	local a01 = mat[ 2]
	local a02 = mat[ 3]
	local a03 = mat[ 4]
    local a10 = mat[ 5]
	local a11 = mat[ 6]
	local a12 = mat[ 7]
	local a13 = mat[ 8]
    local a20 = mat[ 9]
	local a21 = mat[10]
	local a22 = mat[11]
	local a23 = mat[12]
    local a30 = mat[13]
	local a31 = mat[14]
	local a32 = mat[15]
	local a33 = mat[16]
        local a33 = mat[16]

        -- Cache only the current line of the second matrix
        local b0 = mat2[1]
        local b1 = mat2[2]
        local b2 = mat2[3]
        local b3 = mat2[4]
        dest[1] = b0*a00 + b1*a10 + b2*a20 + b3*a30
        dest[2] = b0*a01 + b1*a11 + b2*a21 + b3*a31
        dest[3] = b0*a02 + b1*a12 + b2*a22 + b3*a32
        dest[4] = b0*a03 + b1*a13 + b2*a23 + b3*a33

        b0 = mat2[5]
        b1 = mat2[6]
        b2 = mat2[7]
        b3 = mat2[8]
        dest[5] = b0*a00 + b1*a10 + b2*a20 + b3*a30
        dest[6] = b0*a01 + b1*a11 + b2*a21 + b3*a31
        dest[7] = b0*a02 + b1*a12 + b2*a22 + b3*a32
        dest[8] = b0*a03 + b1*a13 + b2*a23 + b3*a33

        b0 = mat2[9]
        b1 = mat2[10]
        b2 = mat2[11]
        b3 = mat2[12]
        dest[9] = b0*a00 + b1*a10 + b2*a20 + b3*a30
        dest[10] = b0*a01 + b1*a11 + b2*a21 + b3*a31
        dest[11] = b0*a02 + b1*a12 + b2*a22 + b3*a32
        dest[12] = b0*a03 + b1*a13 + b2*a23 + b3*a33

        b0 = mat2[13]
        b1 = mat2[14]
        b2 = mat2[15]
        b3 = mat2[16]
        dest[13] = b0*a00 + b1*a10 + b2*a20 + b3*a30
        dest[14] = b0*a01 + b1*a11 + b2*a21 + b3*a31
        dest[15] = b0*a02 + b1*a12 + b2*a22 + b3*a32
        dest[16] = b0*a03 + b1*a13 + b2*a23 + b3*a33

        return dest
end


function mat4.scale (mat, vec, dest) 
    local x = vec[1]
    local y = vec[2] 
    local z = vec[3]

    if not dest or (mat == dest) then
		mat[1]  =  mat[1]  * x;
		mat[2]  =  mat[2]  * x;
		mat[3]  =  mat[3]  * x;
		mat[4]  =  mat[4]  * x;
		mat[5]  =  mat[5]  * y;
		mat[6]  =  mat[6]  * y;
		mat[7]  =  mat[7]  * y;
		mat[8]  =  mat[8]  * y;
		mat[9]  =  mat[9]  * z;
		mat[10] =  mat[10]  * z;
		mat[11] =  mat[11] *  z;
		mat[12] =  mat[12] *  z;

        return mat;
    end

    dest[1]  = mat[1] * x;
    dest[2]  = mat[2] * x;
    dest[3]  = mat[3] * x;
    dest[4]  = mat[4] * x;
    dest[5]  = mat[5] * y;
    dest[6]  = mat[6] * y;
    dest[7]  = mat[7] * y;
    dest[8]  = mat[8] * y;
    dest[9]  = mat[9] * z;
    dest[10] = mat[10]* z;
    dest[11] = mat[11] * z;
    dest[12] = mat[12] * z;
    dest[13] = mat[13];
    dest[14] = mat[14];
    dest[15] = mat[15];
    dest[16] = mat[16];

    return dest;
end

function mat4.frustum (left, right, bottom, top, near, far, dest)
    if not dest then dest = mat4.create() end

    local rl = (right - left)
    local tb = (top - bottom)
	local fn = (far - near)

    dest[1] = (near * 2.0) / rl;
    dest[2] = 0;
    dest[3] = 0;
    dest[4] = 0;
    dest[5] = 0;
    dest[6] = (near * 2.0) / tb;
    dest[7] = 0;
    dest[8] = 0;
    dest[9] = (right + left) / rl;
    dest[10] = (top + bottom) / tb;
    dest[11] = -(far + near) / fn;
    dest[12] = -1;
    dest[13] = 0;
    dest[14] = 0;
    dest[15] = -(far * near * 2) / fn;
    dest[16] = 0;

    return dest;
end

function mat4.perspective (fovy, aspect, near, far, dest) 
	local top = near * math.tan(fovy * math.pi / 360.0)
	local right = top * aspect

    return mat4.frustum(-right, right, -top, top, near, far, dest)
end


function mat4.ortho (left, right, bottom, top, near, far, dest) 
    if  not dest then dest = mat4.create() end

    local rl = (right - left)
    local tb = (top - bottom)
    local fn = (far - near);

    dest[1] = 2 / rl;
    dest[2] = 0;
    dest[3] = 0;
    dest[4] = 0;
    dest[5] = 0;
    dest[6] = 2 / tb;
    dest[7] = 0;
    dest[8] = 0;
    dest[9] = 0;
    dest[10] = 0;
    dest[11] = -2 / fn;
    dest[12] = 0;
    dest[13] = -(left + right) / rl;
    dest[14] = -(top + bottom) / tb;
    dest[15] = -(far + near) / fn;
    dest[16] = 1;

    return dest;
end

function mat4.fromRotationTranslation (quat, vec, dest) 
    if not dest then dest = mat4.create() end

    local x = quat[1]
    local y = quat[2]
	local z = quat[3]
	local w = quat[4]

	local x2 = x + x
	local y2 = y + y
	local z2 = z + z
	local xx = x * x2
	local xy = x * y2
	local xz = x * z2
	local yy = y * y2
	local yz = y * z2
	local zz = z * z2
	local wx = w * x2
	local wy = w * y2
	local wz = w * z2;

    dest[1] = 1 - (yy + zz);
    dest[2] = xy + wz;
    dest[3] = xz - wy;
    dest[4] = 0;
    dest[5] = xy - wz;
    dest[6] = 1 - (xx + zz);
    dest[7] = yz + wx;
    dest[8] = 0;
    dest[9] = xz + wy;
    dest[10] = yz - wx;
    dest[11] = 1 - (xx + yy);
    dest[12] = 0;
    dest[13] = vec[1];
    dest[14] = vec[2];
    dest[15] = vec[3];
    dest[16] = 1;
    
    return dest;
end

function mat4.transpose (mat, dest)
    -- If we are transposing ourselves we can skip a few steps but have to cache some values
    if not dest or mat == dest then --(!dest || mat === dest) {
        local a01 = mat[2]
        local a02 = mat[3]
        local a03 = mat[4]
        local a12 = mat[7]
        local a13 = mat[8]
        local a23 = mat[12]

        mat[2] = mat[5]
        mat[3] = mat[9]
        mat[4] = mat[13]
        mat[5] = a01
        mat[7] = mat[10]
        mat[8] = mat[14]
        mat[9] = a02
        mat[10] = a12
        mat[12] = mat[15]
        mat[13] = a03
        mat[14] = a13
        mat[15] = a23
        return mat
    end

    dest[1] = mat[1]
    dest[2] = mat[5]
    dest[3] = mat[9]
    dest[4] = mat[13]
    dest[5] = mat[2]
    dest[6] = mat[6]
    dest[7] = mat[10]
    dest[8] = mat[14]
    dest[9] = mat[3]
    dest[10] = mat[7]
    dest[11] = mat[11]
    dest[12] = mat[15]
    dest[13] = mat[4]
    dest[14] = mat[8]
    dest[15] = mat[12]
    dest[16] = mat[16]
    return dest
end

function mat4.translate(mat,vec,dest) 
    local x = vec[1]
    local y = vec[2]
    local z = vec[3]
    local a00, a01, a02, a03,
          a10, a11, a12, a13,
          a20, a21, a22, a23

    if not dest or mat == dest then
        mat[13] = mat[1] * x + mat[5] * y + mat[9] * z + mat[13];
        mat[14] = mat[2] * x + mat[6] * y + mat[10] * z + mat[14];
        mat[15] = mat[3] * x + mat[7] * y + mat[11] * z + mat[15];
        mat[16] = mat[4] * x + mat[8] * y + mat[12] * z + mat[16];

        return mat 
    end

    a00 = mat[1]; a01 = mat[2]; a02 = mat[3]; a03 = mat[4];
    a10 = mat[5]; a11 = mat[6]; a12 = mat[7]; a13 = mat[8];
    a20 = mat[9]; a21 = mat[10]; a22 = mat[11]; a23 = mat[12];

    dest[1] = a00; dest[2] = a01; dest[3] = a02; dest[4] = a03;
    dest[5] = a10; dest[6] = a11; dest[7] = a12; dest[8] = a13;
    dest[9] = a20; dest[10] = a21; dest[11] = a22; dest[12] = a23;

    dest[13] = a00 * x + a10 * y + a20 * z + mat[13];
    dest[14] = a01 * x + a11 * y + a21 * z + mat[14];
    dest[15] = a02 * x + a12 * y + a22 * z + mat[15];
    dest[16] = a03 * x + a13 * y + a23 * z + mat[16];
end


function mat4.rotate(mat, angle, axis, dest)

    local x = axis[1]
    local y = axis[2]
    local z = axis[3]

    local len = math.sqrt(x * x + y * y + z * z)
    local s, c, t,
        a00, a01, a02, a03,
        a10, a11, a12, a13,
        a20, a21, a22, a23,
        b00, b01, b02,
        b10, b11, b12,
        b20, b21, b22;

    if (not len) then
        return nil
    end

    if (len ~= 1) then
        len = 1.0 / len
        x = x * len
        y = y * len
        z = z * len
    end

    s = math.sin(angle)
    c = math.cos(angle)
    t = 1 - c

    a00 = mat[1]; a01 = mat[2]; a02 = mat[3]; a03 = mat[4];
    a10 = mat[5]; a11 = mat[6]; a12 = mat[7]; a13 = mat[8];
    a20 = mat[9]; a21 = mat[10]; a22 = mat[11]; a23 = mat[12];

    -- Construct the elements of the rotation matrix
    b00 = x * x * t + c; b01 = y * x * t + z * s; b02 = z * x * t - y * s;
    b10 = x * y * t - z * s; b11 = y * y * t + c; b12 = z * y * t + x * s;
    b20 = x * z * t + y * s; b21 = y * z * t - x * s; b22 = z * z * t + c;

    if (not dest) then
        dest = mat;
    elseif (mat ~= dest) then -- If the source and destination differ, copy the unchanged last row
        dest[13] = mat[13];
        dest[14] = mat[14];
        dest[15] = mat[15];
        dest[16] = mat[16];
    end

    -- Perform rotation-specific matrix multiplication
    dest[1] = a00 * b00 + a10 * b01 + a20 * b02;
    dest[2] = a01 * b00 + a11 * b01 + a21 * b02;
    dest[3] = a02 * b00 + a12 * b01 + a22 * b02;
    dest[4] = a03 * b00 + a13 * b01 + a23 * b02;

    dest[5] = a00 * b10 + a10 * b11 + a20 * b12;
    dest[6] = a01 * b10 + a11 * b11 + a21 * b12;
    dest[7] = a02 * b10 + a12 * b11 + a22 * b12;
    dest[8] = a03 * b10 + a13 * b11 + a23 * b12;

    dest[9] = a00 * b20 + a10 * b21 + a20 * b22;
    dest[10] = a01 * b20 + a11 * b21 + a21 * b22;
    dest[11] = a02 * b20 + a12 * b21 + a22 * b22;
    dest[12] = a03 * b20 + a13 * b21 + a23 * b22;

    return dest;
end

function mat4.rotateZ(mat, angle, dest) 
    local s = math.sin(angle)
    local c = math.cos(angle)
    local a00 = mat[1]
    local a01 = mat[2]
    local a02 = mat[3]
    local a03 = mat[4]
    local a10 = mat[5]
    local a11 = mat[6]
    local a12 = mat[7]
    local a13 = mat[8]

    if ( not dest) then
        dest = mat;
    elseif mat ~= dest then -- If the source and destination differ, copy the unchanged last row
        dest[9] = mat[9];
        dest[10] = mat[10];
        dest[11] = mat[11];
        dest[12] = mat[12];

        dest[13] = mat[13];
        dest[14] = mat[14];
        dest[15] = mat[15];
        dest[16] = mat[16];
    end

    -- Perform axis-specific matrix multiplication
    dest[1] = a00 * c + a10 * s;
    dest[2] = a01 * c + a11 * s;
    dest[3] = a02 * c + a12 * s;
    dest[4] = a03 * c + a13 * s;

    dest[5] = a00 * -s + a10 * c;
    dest[6] = a01 * -s + a11 * c;
    dest[7] = a02 * -s + a12 * c;
    dest[8] = a03 * -s + a13 * c;

    return dest
end


-------------
-- QUAT4
-------------

function quat4.create( q )
    q = q and q or {}
    local dest = { 0,0,0,1 }

    -- copy data
    for k,v in pairs(q) do
        dest[k] = val
    end

    return dest
end

