require("geometry")
require("glmatrix")

__quadbatch = function( params ) 
	local q 		= __geometry()
		  q.depth 	= -1;
		  q.normal 	= { 0,0,1}
		  q.uv 		= { {0,1},{1,1},{1,0},{0,0}}

	extend(q,params)

	q.buffers.position 	= { size = 3, data = {} }
	q.buffers.normal 	= { size = 3, data = {} }
	q.buffers.uv0 		= { size = 2, data = {} }

	q.begin = function(self)
		self:reset()
	end

	q.reset = function(self)
		self.buffers.position.data = {}
		self.buffers.normal.data = {}
		self.buffers.uv0.data = {}
	end	

	q.add = function(self, x0, y0, x1, y1, x2, y2, x3, y3 )
		if not self.buffers.position then self:reset() end

		local vert 	= self.buffers.position.data
		local norm 	= self.buffers.normal.data
		local uv0  	= self.buffers.uv0.data
		local d 	= self.depth

		local push 	= function( tbl, ...)
			for i,v in ipairs({...}) do 
				table.insert(tbl,v) 
			end
		end

		push( vert, x0, y1, d ); -- v0
		push( vert, x3, y3, d ); -- v3
		push( vert, x2, y2, d ); -- v2
		

		push( vert, x0, y0, d ); -- v0
		push( vert, x2, y2, d ); -- v2
		push( vert, x1, y1, d ); -- v1

		push( norm, self.normal[1], self.normal[2], self.normal[3]); -- n0
		push( norm, self.normal[1], self.normal[2], self.normal[3]); -- n2
		push( norm, self.normal[1], self.normal[2], self.normal[3]); -- n3

		push( norm, self.normal[1], self.normal[2], self.normal[3]); -- n0
		push( norm, self.normal[1], self.normal[2], self.normal[3]); -- n1
		push( norm, self.normal[1], self.normal[2], self.normal[3]); -- n2

		push( uv0, self.uv[1][1], self.uv[1][2]); -- v0
		push( uv0, self.uv[4][1], self.uv[4][2]); -- v3
		push( uv0, self.uv[3][1], self.uv[3][2]); -- v2
		
		push( uv0, self.uv[1][1], self.uv[1][2]); -- v0
		push( uv0, self.uv[3][1], self.uv[3][2]); -- v2
		push( uv0, self.uv[2][1], self.uv[2][2]); -- v1
	end

	q.setcoords = function(self,s0, t0, s1, t1, s2, t2, s3, t3)
		if (t0 == nil) then
			self.uv = s0
			return
		end

		self.uv = { {s0,t0}, 
					{s1,t1},
					{s2,t2},
					{s3,t3} }
	end

	q.setnormals = function(self, n)
		self.normal = { n[1], n[2], n[3] }
	end

	q.addtopleft = function(self, x, y, w, h, r, s )
		if r then 
			local mat = mat4.identity()
			mat4.translate(mat, {x,y,0} )
			mat4.rotateZ(mat,r)

			if s then 
				mat4.scale(mat, {s,s,s})
			end

			local v0 = mat4.multiplyVec3( mat, { 0,0,0 } )
			local v1 = mat4.multiplyVec3(mat, {   w, 0.0, 0.0});
			local v2 = mat4.multiplyVec3(mat, {   w,   h, 0.0});
			local v3 = mat4.multiplyVec3(mat, { 0.0,   h, 0.0});

			self:add( v0[1],v0[2], v1[1],v1[2],v2[1],v2[2],v3[1],v3[2] ) -- gulp
		else 
			self:add( x    ,y    ,
				      x + w,y    ,
				      x + w,y + h,
				      x    ,y + h )
		end
	end

	q.addcentered = function(self, x,y,w,h,r,s)
		if r then 
			local mat = mat4.identity()
			mat4.translate(mat,{x,y,0})
			mat4.rotateZ(mat,r)

			if s then 
				mat4.scale(mat,{s,s,s})
			end
			local v0 = mat4.multiplyVec3( mat, {-w/2.0, -h/2.0, 0.0} )
			local v1 = mat4.multiplyVec3( mat, { w/2.0, -h/2.0, 0.0} )
			local v2 = mat4.multiplyVec3( mat, { w/2.0,  h/2.0, 0.0} )
			local v3 = mat4.multiplyVec3( mat, {-w/2.0,  h/2.0, 0.0} )

			self:add( v0[1],v0[2], v1[1],v1[2],v2[1],v2[2],v3[1],v3[2] ) -- gulp
		else 
			x = x - w / 2
			y = y - h / 2

			self:add( x    ,y    ,
				      x + w,y    ,
				      x + w,y + h,
				      x    ,y + h )
		end
	end

	q.finish = function(self)
		self:build()
	end

	q.superdraw = q.draw

	q.draw = function( self, shader, bindbuffers )
		if not bindbuffers then 
			bindbuffers = { 
				position = self.buffers.position and true,
				normal = self.buffers.normal and true, 
				uv0 = self.buffers.uv0 and true }
			end

		self:bind(shader,bindbuffers)
		self:superdraw()
		self:unbind(shader,bindbuffers)
	end

	return q
end
